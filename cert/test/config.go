package main

import (
	"fmt"
	core "k8s.io/api/core/v1"
	"sigs.k8s.io/yaml"
)

func main() {
	a := core.ConfigMap{
		Data: make(map[string]string),
	}
	a.Kind = "ConfigMap"
	a.APIVersion = "v1"
	a.Namespace = "default"
	a.Name = "esconfig"
	a.Data["elasticsearch.yaml"] = "" +
		"network.host: 0.0.0.0\n" +
		"xpack.security.enabled: true\n" +
		"xpack.security.transport.ssl.enabled: true\n" +
		"xpack.security.transport.ssl.verification_mode: certificate\n" +
		"xpack.security.transport.ssl.keystore.path: ec.p12\n" +
		"xpack.security.transport.ssl.truststore.path: ec.p12\n" +
		"xpack.security.http.ssl.enabled: true\n" +
		"xpack.security.http.ssl.verification_mode: certificate\n" +
		"xpack.security.http.ssl.keystore.path: ec.p12\n" +
		"xpack.security.http.ssl.truststore.path: ec.p12\n" +
		"xpack.security.audit.enabled: true"
	marshal, err := yaml.Marshal(&a)
	if err != nil {
		return
	}
	fmt.Println(string(marshal))

}
