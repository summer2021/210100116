openssl pkcs12 -in ec.p12 -out ec.crt -nokeys -clcerts
openssl pkcs12 -in ec.p12 -nocerts -nodes -out ec.key

kubectl delete secret ca-secret -n sw
kubectl delete secret tls-secret -n sw

kubectl create secret generic ca-secret --from-file=ca.crt=ec.crt -n sw

kubectl create secret generic tls-secret --from-file=tls.crt=ec.crt --from-file=tls.key=ec.key -n sw


