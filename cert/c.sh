openssl req -x509 -sha256 -newkey rsa:4096 -keyout ca.key -out ca.crt -days 356 -nodes -subj '/CN=dashboard.kube.com'

openssl req -new -newkey rsa:4096 -keyout server.key -out server.csr -nodes -subj  '/CN=dashboard.kube.com'

openssl x509 -req -sha256 -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt

kubectl delete secret ca-secret -n sw
kubectl delete secret tls-secret -n sw

kubectl create secret generic ca-secret --from-file=ca.crt=ca.crt -n sw

kubectl create secret generic tls-secret --from-file=tls.crt=server.crt --from-file=tls.key=server.key -n sw
