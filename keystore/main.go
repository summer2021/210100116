package main

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/pem"
	"fmt"
	"golang.org/x/crypto/pkcs12"
	"io/ioutil"
	certv1beta1 "k8s.io/api/certificates/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	npkcs12 "software.sslmate.com/src/go-pkcs12"
)

type SliceMock struct {
	addr uintptr
	len  int
	cap  int
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}
func main() {
	//if len(os.Args) < 3 { // nolint: gomnd
	//	log.Fatal("usage: <path> <password>")
	//}
	//b,_:=ioutil.ReadFile("/Users/apple/GolandProjects/210100116/keystore/storage.p12")
	b, _ := ioutil.ReadFile("keystore/storage.p12")
	fmt.Println(1, len(b))
	//_,cert3,_:=pkcs12.Decode(b,"")
	key2, cert2, err := pkcs12.Decode(b, "")
	fmt.Println(cert2.Subject.CommonName)
	//if err!=nil {
	//	fmt.Println(err)
	//}
	key, _ := rsa.GenerateKey(rand.Reader, 4096)
	keybuffer := new(bytes.Buffer)
	pem.Encode(keybuffer, &pem.Block{
		Bytes: x509.MarshalPKCS1PrivateKey(key),
		Type:  "RSA PRIVATE KEY",
	})
	//var oidEmailAddress = asn1.ObjectIdentifier{1, 2, 840}
	//emailAddress := "skywalking@example.com"
	subj := pkix.Name{
		CommonName:         "skywalking-storage",
		Country:            []string{"CN"},
		Province:           []string{"ZJ"},
		Locality:           []string{"HZ"},
		Organization:       []string{"ZJU"},
		OrganizationalUnit: []string{"ZJU"},
	}
	rawSubj := subj.ToRDNSequence()
	//rawSubj = append(rawSubj, []pkix.AttributeTypeAndValue{
	//	{Type: oidEmailAddress, Value: emailAddress},
	//})
	asn1Subj, _ := asn1.Marshal(rawSubj)
	template := x509.CertificateRequest{
		RawSubject:         asn1Subj,
		SignatureAlgorithm: x509.SHA256WithRSA,
	}
	csrBytes, _ := x509.CreateCertificateRequest(rand.Reader, &template, key)
	buffer := new(bytes.Buffer)
	err = pem.Encode(buffer, &pem.Block{Type: "CERTIFICATE REQUEST", Bytes: csrBytes})
	if err != nil {
		return
	}
	fmt.Println(buffer)
	//encodeString := base64.StdEncoding.EncodeToString(buffer.Bytes())
	//fmt.Println(encodeString)
	fmt.Println("buffer:", buffer)

	singername := "kubernetes.io/kube-apiserver-client"
	request := certv1beta1.CertificateSigningRequest{
		TypeMeta: metav1.TypeMeta{
			Kind:       "CertificateSigningRequest",
			APIVersion: "certificates.k8s.io/v1beta1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name: "userv",
		},
		Spec: certv1beta1.CertificateSigningRequestSpec{
			Groups:     []string{"system:authenticated"},
			Request:    []byte(buffer.String()),
			SignerName: &singername,
			Usages:     []certv1beta1.KeyUsage{certv1beta1.UsageClientAuth},
		},
	}
	// use the current context in kubeconfig
	kubeconfig := "/Users/apple/.kube/config"
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err.Error())
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	ctx := context.Background()
	err = clientset.CertificatesV1beta1().CertificateSigningRequests().Delete(ctx, "userv", metav1.DeleteOptions{})
	if err != nil {
		fmt.Println("error")
	}
	csr, err := clientset.CertificatesV1beta1().CertificateSigningRequests().Create(ctx, &request, metav1.CreateOptions{})

	if err != nil {
		fmt.Println(err)
	}
	condition := certv1beta1.CertificateSigningRequestCondition{
		Type:    "Approved",
		Reason:  "ApprovedByMyPolicy",
		Message: "Approved by my custom approver controller",
	}
	csr.Status.Conditions = append(csr.Status.Conditions, condition)
	csrupdate, err := clientset.CertificatesV1beta1().CertificateSigningRequests().UpdateApproval(ctx, csr, metav1.UpdateOptions{})
	if err != nil {
		fmt.Println(err, csrupdate)
	}
	csr, err = clientset.CertificatesV1beta1().CertificateSigningRequests().Get(ctx, "userv", metav1.GetOptions{})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(csr.Status.Certificate))
	_ = csr.Status.Certificate
	//a:=[]byte("LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURUakNDQWphZ0F3SUJBZ0lSQUxIekRiWEZ6YThGd00vNXB4RkdMTll3RFFZSktvWklodmNOQVFFTEJRQXcKRlRFVE1CRUdBMVVFQXhNS2EzVmlaWEp1WlhSbGN6QWVGdzB5TVRBNE1EZ3dNek0zTlRCYUZ3MHlNakE0TURndwpNek0zTlRCYU1Hd3hDekFKQmdOVkJBWVRBa0ZWTVJNd0VRWURWUVFJRXdwVGIyMWxMVk4wWVhSbE1ROHdEUVlEClZRUUhFd1pOZVVOcGRIa3hGREFTQmdOVkJBb1RDME52YlhCaGJua2dUSFJrTVFzd0NRWURWUVFMRXdKSlZERVUKTUJJR0ExVUVBeE1MWlhoaGJYQnNaUzVqYjIwd2dnRWlNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0SUJEd0F3Z2dFSwpBb0lCQVFDek1BUUhTQ1pBNWh3OVlwc0YxWnNTSEpxLzNnZDFjNmVqZURyOWtFcEhRRjFTVjE5Z3ZRUS94MkdBCjZOWW5IU2NUYzFwbmlmNTJsL0RJODFYdUlDT1B3SjJUMWMwTXJiU3YyaFRleHFHVkc2elpIL2ExZjBOSWNRakEKRHZLTHFkNkJNbmIzVzFCM0ljY3k3aXFuRXJ0WW1MYlBQbGpMVnUwWDc2dmRGa2N3L2ZMTC9JRm9NeXRCSkNrbQpLVExtdVJ1bzNXazFvOHZDUDZBTlYvRGI1enNCUzBlcy9raDlXRzFpb0g0aW1ITi9CeXZpeWROSHVOSFJJZlFRCkk4NnVnd2pCdFQra1hpcHdXZS9wNGV0NjF0NU1QeHRKRlJYTEN2R3N6UktrUWUwLy81MDhsaEpmbmtRSTI3amYKbm15UkVOaDRoT29JUGpVSzFXMDRobVNwZm4weEFnTUJBQUdqUWpCQU1CTUdBMVVkSlFRTU1Bb0dDQ3NHQVFVRgpCd01DTUF3R0ExVWRFd0VCL3dRQ01BQXdHd1lEVlIwUkJCUXdFb0VRZEdWemRFQmxlR0Z0Y0d4bExtTnZiVEFOCkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQUNSNXp5Zm0xc2tNNml6Yk03UmZMYTd6UnAvRXE2anZwb1c5dUxvS1MKeXVVVWJ5MW1RdGNHN1V4RlFKQy9DcjI0WUZoNXNlcnd5Ymo4R3dMUlM5dUdTVFdndnhPZVlYNms5OGF1dm9UMwpPWXBta2Y2NW1NeUJ3eit3dXJJSjBzNXgzZXlhWm01cDBQdDZvb0lzVUdZNDd5MjlzTE0vYTQ4dGZjdHd4Wnk0CkV2Q0pXbTZSL0tsejJRZHYwaWx5K1k5WUtZdHFoV21FamxlaWdvRnI4TFZvUkNsTFVCeVJEbXZVU1hmUzllWm4KUWxlZDduMEhpRGI2dGU3RHBueEVoZnRrN1A3eGNhYlRYSlZCK3pzZ0E0M0paRTFEVEhKajE3VE5icVlob1FFNQpvN0dRUmNUbHZCMWQ2QXZ4ZDB3MjdJd21EWWM1Wm9tdzkxRUJxbDNIQXB5bFp3PT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo")
	//	certificate, err := ssh.Pas(a)
	fmt.Println(string(csr.Status.Certificate))
	block, _ := pem.Decode(csr.Status.Certificate)

	cert, err := x509.ParseCertificate(block.Bytes)
	fmt.Println(cert.Subject)
	//create pkcs12
	p12, err := npkcs12.Encode(rand.Reader, key, cert, nil, "")
	err = ioutil.WriteFile("keystore/storage-self.p12", p12, 0755)
	if err != nil {
		fmt.Println(err)
	}
	//encodeString := base64.StdEncoding.EncodeToString(p12)
	//fmt.Println("p12 base64",encodeString)
	key2, cert2, _ = pkcs12.Decode(p12, "")
	fmt.Println(cert2.Subject.CommonName)
	fmt.Println(key2)

	//然后创建secret就可以了
	//	data:=make(map[string][]byte)
	//	data["storage.p12"]=p12
	//	secret:=core.Secret{
	//		TypeMeta: metav1.TypeMeta{
	//			Kind:       "Secret",
	//			APIVersion: "v1",
	//		},
	//		ObjectMeta: metav1.ObjectMeta{
	//			Name:      "sssss",
	//			Namespace: "default",
	//		},
	//		Data:       data,
	//		Type:       "Opaque",
	//	}
	//	create, err := clientset.CoreV1().Secrets("default").Create(ctx, &secret, metav1.CreateOptions{})
	//	if err != nil {
	//		return
	//	}
	//	fmt.Println(create.Data)
}
