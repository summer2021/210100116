module github.com/apache/skywalking-swck

go 1.14

require (
	github.com/Masterminds/sprig/v3 v3.1.0
	github.com/apache/skywalking-cli v0.0.0-20210209032327-04a0ce08990f
	github.com/evanphx/json-patch v4.9.0+incompatible
	github.com/ghodss/yaml v1.0.0
	github.com/go-logr/logr v0.3.0
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/google/gofuzz v1.1.1-0.20200604201612-c04b05f3adfa // indirect
	github.com/google/uuid v1.1.5 // indirect
	github.com/hashicorp/golang-lru v0.5.5-0.20210104140557-80c98217689d // indirect
	github.com/kubernetes-sigs/custom-metrics-apiserver v0.0.0-20201110135240-8c12d6d92362
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli v1.22.1
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20210420205809-ac73e9fd8988 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	honnef.co/go/tools v0.1.3 // indirect
	k8s.io/api v0.20.1
	k8s.io/apimachinery v0.20.1
	k8s.io/apiserver v0.20.1
	k8s.io/client-go v0.20.1
	k8s.io/component-base v0.20.1
	k8s.io/klog/v2 v2.4.0
	k8s.io/metrics v0.20.1
	sigs.k8s.io/controller-runtime v0.7.0
	sigs.k8s.io/yaml v1.2.0
	software.sslmate.com/src/go-pkcs12 v0.0.0-20210415151418-c5206de65a78
)

replace github.com/googleapis/gnostic => github.com/googleapis/gnostic v0.4.1

replace skywalking/network => github.com/apache/skywalking-cli/gen-codes/skywalking/network v0.0.0-20210209032327-04a0ce08990f

replace google.golang.org/grpc => google.golang.org/grpc v1.29.1
